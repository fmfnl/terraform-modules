# Adding a null resource that just serves as a means to upload the
# config file to Proxmox. It also asks the Proxmox server to generate
# a secret ID for the AppRole authentication of the VM. This way, no
# party has both parts of the VM credentials.
#
# This will run automatically if the flatcar config changes, but it won't
# run when only the VM is replaced (by hand for example). That is a bit
# unfortunate, but otherwise it would cause a loop. See also
# https://github.com/hashicorp/terraform/issues/31707
resource "null_resource" "config_provisioning" {
    # Only provision after the flatcar config is known
    depends_on = [
        data.ct_config.flatcar_config
    ]

    # Re-do the provisioning when the flatcar config has changed, but then
    # we do not yet replace the VM. We replace the VM when manually tainted
    # or when this input variable has changed.
    triggers = {
        content = sha1(data.ct_config.flatcar_config.rendered)
        input_observable = var.replace_on
    }

    # Specify SSH connection to Proxmox server
    connection {
        type = "ssh"

        host = var.provisioning_config.ssh.hostname
        user = var.provisioning_config.ssh.username
        
        agent = false
        private_key = local.private_key
        certificate = local.certificate
    }

    # Deploy the flatcar config file
    provisioner "file" {
        content = data.ct_config.flatcar_config.rendered
        destination = local.config_file
    }

    # Run the secret creation
    provisioner "remote-exec" {
        inline = [
            join(" ", [ # Join is the only way to do multiline splitting of contiguous strings
                "vault write",

                # We want a wrapping token instead of a plaintext secret
                "-wrap-ttl=360s",
                "-field=wrapping_token",

                # Generated secret ID parameters
                "auth/approle/role/${var.approle_role}/secret-id",
                "secret_id_ttl=360s",

                # Bind the secret and later token to only be accessed by the VM
                join(" ", [for net in var.networks : "secret_id_bound_cidrs=${net.address}/32"]),
                join(" ", [for net in var.networks : "token_bound_cidrs=${net.address}/32"]),

                # Print the resulting token in the proper file on the server
                "> ${local.secret_file}"
            ])
        ]
    }
}

resource "proxmox_vm_qemu" "flatcar_vm" {
    depends_on = [
        null_resource.config_provisioning,
        data.ct_config.flatcar_config
    ]

    # Replace the VM when this marker variable has changed value. This prevents
    # manual tainting and such.
    force_recreate_on_change_of = var.replace_on

    name = var.name
    vmid = var.id
    target_node = var.node

    desc = var.description
    args = join(" ", [
        "-fw_cfg name=opt/org.flatcar-linux/config,file=${local.config_file}",
        "-fw_cfg name=opt/nl.fmf.terraform/secrettoken,file=${local.secret_file}"
    ])

    clone = var.template
    full_clone = true

    memory = var.memory.maximum
    balloon = var.memory.minimum == null ? 0 : var.memory.minimum
    cores = var.core_count
    scsihw = "virtio-scsi-pci"

    # Autostart config
    onboot = var.autostart != null
    startup = join(",", compact([var.autostart.order == null ? "" : "order=${var.autostart.order}", var.autostart.timeout == null ? "" : "up=${var.autostart.timeout}"]))

    agent = 1
    ci_wait = 0

    dynamic "network" {
        for_each = var.networks
        content {
            model = "virtio"
            bridge = network.value.bridge_name
            firewall = true
        }
    }

    dynamic "disk" {
        for_each = var.drives
        content {
            type = "scsi"
            storage = disk.value.backing_storage
            size = disk.value.size
        }
    }

    timeouts {
        create = "1s"
    }
}

# A random UUID corresponding to a "deployment" of the VM. When the VM is recreated,
# the UUID will be regenerated, which can be used to redeploy some other resources
# outside of the module.
resource "random_uuid" "vm_instance" {
    keepers = {
        vm_id = proxmox_vm_qemu.flatcar_vm.id
    }
}
